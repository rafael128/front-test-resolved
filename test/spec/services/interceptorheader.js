'use strict';

describe('Service: interceptorHeader', function () {

  // load the service's module
  beforeEach(module('frontTestApp'));

  // instantiate service
  var interceptorHeader;
  beforeEach(inject(function (_interceptorHeader_) {
    interceptorHeader = _interceptorHeader_;
  }));

  it('should do something', function () {
    expect(!!interceptorHeader).toBe(true);
  });

});
