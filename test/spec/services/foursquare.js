'use strict';

describe('Service: Foursquare', function () {

  // load the service's module
  beforeEach(module('frontTestApp'));

  // instantiate service
  var Foursquare;
  beforeEach(inject(function (_Foursquare_) {
    Foursquare = _Foursquare_;
  }));

  it('should do something', function () {
    expect(!!Foursquare).toBe(true);
  });

});
