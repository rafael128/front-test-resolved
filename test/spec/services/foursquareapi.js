'use strict';

describe('Service: foursquareApi', function () {

  // load the service's module
  beforeEach(module('frontTestApp'));

  // instantiate service
  var foursquareApi;
  beforeEach(inject(function (_foursquareApi_) {
    foursquareApi = _foursquareApi_;
  }));

  it('should do something', function () {
    expect(!!foursquareApi).toBe(true);
  });

});
