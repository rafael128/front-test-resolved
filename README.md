# Softruck Frontend Engineer Test

Implement a simple website that consumes [Foursquare's API](https://developer.foursquare.com/) to get places and show them in a map. The map view can be changed by the users allowing them to respond to the following queries:

- View all places with certain category within a certain distance from the user (user must input the category and distance values)
- View a heat map layer of places in a city specified by the user (a zone with many places will be red, a zone with few places will be green)
- View the top five of places according to the amount of checkins near the user's current location

The UI and UX are completely in your hands, so you will have to make sensible decisions about it.  Also, pay attention to the quality and clarity of your code and the extensibility of your proposed solution. These are the things that will make you stand out from the crowd.

Although we aim for an AngularJS application, your solution can be implemented any tool that you find useful or feel comfortable with. Here is a list of possible tools and technologies that we prefer:

- SASS or LESS (or any other CSS preprocessor)
- Gulp or Grunt
- Bower
- Mapbox

The code can be open-sourced in a public repository as the origin of the data used is also public. If you choose to make it private, please provide the necessary credentials to access it. You can also make a public or private fork of this repository.

The deadline for delivering your solution is **exactly one week from the date you received the test**.

In case you run into any trouble or doubts, contact Mathias Fonseca at +55 31 9944-7878 or <mathias.fonseca@softruck.com>.
