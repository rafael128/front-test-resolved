'use strict';

/**
 * @ngdoc overview
 * @name frontTestApp
 * @description
 * # frontTestApp
 *
 * Main module of the application.
 */
angular
  .module('frontTestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'LocalStorageModule',
    'siyfion.sfTypeahead'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/authentication', {
        templateUrl: 'views/authentication.html',
        controller: 'AuthenticationCtrl',
        controllerAs: 'authentication'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
