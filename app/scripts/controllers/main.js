
/**
 * @ngdoc function
 * @name frontTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontTestApp
 */
(function(){
	'use strict';

	function MainCtrl($scope, localStorageService, authentication, foursquareApi, Foursquare) {
		L.mapbox.accessToken = 'pk.eyJ1IjoicmFmZXgxMjgiLCJhIjoiY2loNDg5MjZrMHlqNzY1bTAwYW1keWFsZyJ9._AUpoFi4JkrDa7565iHUOw';
		var map = L.mapbox.map('map', 'mapbox.streets');
		$scope.searchInGeo = {};
		$scope.show = "";
		$scope.user = [];
		$scope.myLocation = {};
		if (!localStorageService.get("UserToken")) {
			authentication.authenticationFoursquare();
		}
		foursquareApi.getUser(function (data){
			$scope.user = data.response.user;
			foursquareApi.getSearch({venue: "search", near: $scope.user.homeCity}, function (response){
				$scope.myLocation.lat=response.response.geocode.feature.geometry.center.lat;
				$scope.myLocation.lng=response.response.geocode.feature.geometry.center.lng;
				map.setView([response.response.geocode.feature.geometry.center.lat, response.response.geocode.feature.geometry.center.lng], 9);
			});	
		});

		var search = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.title[0]); },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: Foursquare.Api.path("search/autocomplete", localStorageService.get("UserToken"), "&group=unified"),
				replace: function(url, query){
					return url+"&query="+query
				},
				filter: function(data){
					return data.response.groups[0].items;
				},
			},
			limit: 6
		});
		var searchCity = new Bloodhound({
			datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.title[0]); },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: Foursquare.Api.path("geo/geocode", localStorageService.get("UserToken"), "&autocomplete=true"),
				replace: function(url, query){
					return url+"&query="+query
				},
				filter: function(data){
					return data.response.geocode.interpretations.items;
				},
			},
			limit: 6
		});
		searchCity.initialize();
		search.initialize();
		$scope.options = {
			displayKey: function (argument) {
				return argument.text;
			},
			source: search.ttAdapter(),
			templates: {
				empty: function(){
					angular.element('.moore-result').remove();
					return '<div class="empty-message">No results found</div>';
				},
			}	
		};
		$scope.optionsByCity = {
			displayKey: function (argument) {
				return argument.feature.name;
			},
			source: searchCity.ttAdapter(),
			templates: {
				empty: function(){
					angular.element('.moore-result').remove();
					return '<div class="empty-message">No results found</div>';
				},
			}	
		};
		setTimeout(function(){
			angular.element('.dropdown-button').dropdown({
				constrain_width: false,
				hover: true,
				alignment: 'left'
			}, 300);
		})
		$scope.search = function(){
			var foursquarePlaces = L.layerGroup().addTo(map);
			if ($scope.show==="primer") {
				foursquareApi.getLocations({near: $scope.user.homeCity, llAcc: $scope.searchInGeo.km, query: $scope.searchInGeo.category.text}, function (data){
					for (var i = 0; i < data.response.venues.length; i++) {
						var venue = data.response.venues[i];
						L.marker(L.latLng(venue.location.lat, venue.location.lng))
						.bindPopup('<strong><a href="https://foursquare.com/v/' + venue.id + '">'+venue.name + '</a></strong>')
						.addTo(foursquarePlaces);
					}
				});
			}else if ($scope.show==="second"){
				map.setView([$scope.searchInGeo.city.feature.geometry.center.lat, $scope.searchInGeo.city.feature.geometry.center.lng], 9);
				foursquareApi.getByCity({near: $scope.searchInGeo.city.feature.name}, function (data){
					setTimeout(function(){
						for (var i = 0; i < data.response.venues.length; i++) {
							var venue = data.response.venues[i];
							L.marker(L.latLng(venue.location.lat, venue.location.lng))
							.bindPopup('<strong><a href="https://foursquare.com/v/' + venue.id + '">'+venue.name + '</a></strong>')
							.addTo(foursquarePlaces);

						}
					},300)
				});
			}else{
				map.setView([$scope.myLocation.lat, $scope.myLocation.lng], 9);
				foursquareApi.getByChekin({venue: "search", near: $scope.user.homeCity}, function (response){
					for (var i = 0; i < data.response.venues.length; i++) {
						var venue = data.response.venues[i];
						L.marker(L.latLng(venue.location.lat, venue.location.lng))
						.bindPopup('<strong><a href="https://foursquare.com/v/' + venue.id + '">'+venue.name + '</a></strong>')
						.addTo(foursquarePlaces);
					}
				});
			}
		};
		$scope.changeShow = function(show){
			$scope.show = show;
		};

	}
	angular.module('frontTestApp').controller('MainCtrl', MainCtrl);

})();