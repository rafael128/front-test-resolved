'use strict';

/**
 * @ngdoc function
 * @name frontTestApp.controller:AuthenticationCtrl
 * @description
 * # AuthenticationCtrl
 * Controller of the frontTestApp
 */

 (function(){
 	function AuthenticationCtrl(localStorageService, utils, $location){
 		if (utils.getHashParameter("access_token")) {
 			localStorageService.set("UserToken", utils.getHashParameter("access_token"));
 			$location.path("/")
 		}
 	}
	angular.module('frontTestApp')
	.controller('AuthenticationCtrl', AuthenticationCtrl);
 })();
