'use strict';

/**
 * @ngdoc service
 * @name frontTestApp.foursquareApi
 * @description
 * # foursquareApi
 * Factory in the frontTestApp.
 */

(function() {
  function foursquareApi($resource, Foursquare, localStorageService){
    return $resource("","", {
      getUser: {
        url: Foursquare.Api.path("users/self", localStorageService.get("UserToken"), ""),
        method: "GET"
      },
      getVenue: {
        url: Foursquare.Api.path("venues/:venue", localStorageService.get("UserToken"), ""),
        method: "GET",
        params: {
          venue: "@venue"
        }
      },
      getByChekin: {
        url: Foursquare.Api.path("venues/:venue", localStorageService.get("UserToken"), "&near=:near&intent=checkin&limit=5"),
        method: "GET",
        params: {
          venue: "@venue",
          near: "@near"
        }
      },
      getSearch: {
        url: Foursquare.Api.path("venues/:venue", localStorageService.get("UserToken"), "&near=:near"),
        method: "GET",
        params: {
          venue: "@venue",
          near: "@near"
        }
      },
      getLocations: {
        url: Foursquare.Api.path("venues/search", localStorageService.get("UserToken"), "&near=:near&llAcc=:llAcc&query=:query"),
        method: "GET",
        params: {
          near: "@near",
          llAcc: "@llAcc",
          query: "@query"
        }
      },
      getByCity: {
        url: Foursquare.Api.path("venues/search", localStorageService.get("UserToken"), "&near=:near"),
        method: "GET",
        params: {
          near: "@near"
        }
      }      
    });
  }

  angular.module('frontTestApp').factory('foursquareApi', foursquareApi);
})();