'use strict';

/**
 * @ngdoc service
 * @name frontTestApp.Foursquare
 * @description
 * # Foursquare
 * Constant in the frontTestApp.
 */
(function() {
	var Foursquare = {
		clientId: 			'Y5MISWVV3RXTTXQA5QYJUSNYZY4XON2ZCPHVAZGY2YUKSDKI',
		clientSecret: 		'X0CA3YFGT4BSCG4OYR1W1ALDRHCTN10DC12N21VFX2ZCVJJU',
		urlGetAccessToken: 	'https://foursquare.com/oauth2/authenticate?client_id={0}&response_type={1}&redirect_uri={2}',
		Api: {
			url: 'https://api.foursquare.com/',
			version: 'v2',
			v: '20151118',
			m: 'foursquare',
			path: function(apiPath, token, beforeParameter){
				return this.url + this.version +"/"+ apiPath+"?oauth_token="+token+"&v="+this.v+"&m="+this.m+beforeParameter;
			}
		}
	};
	angular.module('frontTestApp').constant('Foursquare', Foursquare);
})();