'use strict';

/**
 * @ngdoc service
 * @name frontTestApp.utils
 * @description
 * # utils
 * Factory in the frontTestApp.
 */

(function(){
	String.prototype.format = String.prototype.f = function() {
		var s = this,
		i = arguments.length;

		while (i--) {
			s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
		}
		return s;
	};	
	  function utils($location){
	  	//if have params at type http://exempl.com#parameter=asdas
	  	function _getHashParameter(param){
	  		return $location.hash().split(param+"=")[1].split("&")[0];
	  	}
	  	return{
	  		getHashParameter: _getHashParameter
	  	};
	  }
	  angular.module('frontTestApp')
	  .factory('utils', utils);
  
})();