'use strict';

/**
 * @ngdoc service
 * @name frontTestApp.authentication
 * @description
 * # authentication
 * Factory in the frontTestApp.
 */

(function(){
  function authentication($location, Foursquare, $window){
    function _authenticationFoursquare(){
      $window.location.href = Foursquare.urlGetAccessToken.format(Foursquare.clientId, "token", "http://localhost:9000/authentication");
    }
    return {
      authenticationFoursquare: _authenticationFoursquare
    };
  }
  angular.module('frontTestApp')
  .factory('authentication', authentication);
  
})();